package wav2lipEx

import (
	"context"
	"gitee.com/kaylee595/go-wav2lipEx/services/ffmpeg"
	"gitee.com/kaylee595/go-wav2lipEx/services/gfpgan"
	"gitee.com/kaylee595/go-wav2lipEx/services/python"
	"gitee.com/kaylee595/go-wav2lipEx/services/wav2Lip"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestWav2lipEx(t *testing.T) {
	p := python.New("C:/miniconda/python.exe")
	w, err := New(
		WithWav2Lip(wav2Lip.New(p, "./bin/Wav2Lip")),
		WithGfpgan(gfpgan.New(p, "./bin/GFPGAN-1.3.8")),
		WithFfmpeg(ffmpeg.New("C:\\Users\\kayleeo\\software\\ffmpeg-gpl\\ffmpeg.exe",
			ffmpeg.WithLoglevel(ffmpeg.LogLevelError),
		)),
	)
	require.NoError(t, err)
	input := "C:\\Users\\kayleeo\\Desktop\\test\\1.mp4"
	audio := "C:\\Users\\kayleeo\\Desktop\\test\\1.mp3"
	output := "test.mp4"
	err = w.GenerateVideo(context.Background(), input, audio, output, Paras{})
	require.NoError(t, err)
}
