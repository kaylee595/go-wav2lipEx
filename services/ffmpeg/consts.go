package ffmpeg

type Loglevel int

const (
	// LogLevelQuiet Show nothing at all; be silent.
	LogLevelQuiet Loglevel = -8
	// LogLevelPanic Only show fatal errors which could lead the process to crash, such as an assertion failure. This is not currently used for anything.
	LogLevelPanic Loglevel = 0
	// LogLevelFatal Only show fatal errors. These are errors after which the process absolutely cannot continue.
	LogLevelFatal Loglevel = 8
	// LogLevelError Show all errors, including ones which can be recovered from.
	LogLevelError Loglevel = 16
	// LogLevelWarning Show all warnings and errors. Any message related to possibly incorrect or unexpected events will be shown.
	LogLevelWarning Loglevel = 24
	// LogLevelInfo Show informative messages during processing. This is in addition to warnings and errors. This is the default value.
	LogLevelInfo Loglevel = 32
	// LogLevelVerbose Same as info, except more verbose.
	LogLevelVerbose Loglevel = 40
	// LogLevelDebug Show everything, including debugging information.
	LogLevelDebug Loglevel = 48
	LogLevelTrace Loglevel = 56
)
